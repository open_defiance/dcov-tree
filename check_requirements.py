import pkg_resources
_ = input('Atenção: este script vai substituir o seu requirements.txt com as versões atualmente disponíveis na máquina.\n Aperte ENTER para continuar ou Ctrl-C para cancelar')

with open('requirements_check.txt') as handle: lines = handle.readlines()

versions = []
for line in [line.strip('\n') for line in lines]:
    version = ('{}={}'.format(line,pkg_resources.get_distribution(line).version))
    versions.append(version)
    print(version)
    
with open('requirements.txt', 'w') as handle:
    handle.write('\n'.join(versions))
