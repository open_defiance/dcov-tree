from numba import jit
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import dcor
import itertools
from math import ceil, sqrt
from collections import OrderedDict
from scipy.sparse import issparse
from sklearn.covariance import empirical_covariance, graphical_lasso, oas
from sklearn.preprocessing import scale
def factorgen(n):
    if n <= 1: return
    prime = next((x for x in range(2, ceil(sqrt(n))+1) if n%x == 0), n)
    yield prime
    yield from factorgen(n//prime)

def rectangle(n):
    if n % 2 == 0:
        ncols = 2
        nrows = n//2
    else:
        ncols = 3
        nrows = ceil(n/3)
    return nrows, ncols

@jit
def xlogx(x): return x*np.log(x)

@jit
def xmean(x): return (x - np.mean(x))**2
@jit
def colwise(func, matrix1, matrix2):
    assert matrix1.shape == matrix2.shape, "Matrices must be same size"
    nrow, ncol = matrix1.shape
    results = np.zeros((ncol,ncol))
    for u in range(ncol):
        for v in range(ncol):
            results[u,v] = func(matrix1[:,u],matrix2[:,v])
    return results

@jit
def minmax_scale(vec):
    vec = np.array(list(vec))
    m, M = np.min(vec),np.max(vec)
    return (vec - m)/(M-m)

def _correlation(data, corr_mode,  corr_type,alpha):
    if corr_type == 'distance':
        func = dcor.u_distance_correlation_sqr
        matrix = colwise(func, data, data)
    elif corr_type == 'glasso':
        assume_centered = issparse(data)
        if not issparse(data): data = scale(data)
        emp = empirical_covariance(data, assume_centered)

        try:
            lasso =  graphical_lasso(emp,alpha, mode='cv')
            matrix = lasso[0]
        except FloatingPointError:
            matrix = emp
    matrix = minmax_scale(matrix)
    if corr_mode == 'similarity':
        return np.sqrt(1 - matrix)
    elif corr_mode == 'cost':
        return np.log(matrix)
    elif corr_mode == 'regression':
        return xmean(matrix)
    elif corr_mode == 'entropy':
        return -xlogx(matrix)
class CorrelationTree:
    def __init__(self,data, labels = None, corr_type = 'distance', corr_mode = 'similarity', alpha = 1e-5, seed = 0):
        assert corr_type in ['glasso','distance'], 'Invalid corr_type, choose glasso or distance'
        assert corr_mode in ['similarity','cost','regression','entropy'], 'Choose corr_mode = similarity, cost, regression or entropy'
        self.corr_type = corr_type
        self.corr_mode = corr_mode
        self.seed = seed
        self.graphical_alpha = alpha
        if labels is None:
            labels = list(range(data.shape[1]))
        self.labels = labels
        self.set_corr_tree(data)
        self.T_node_weight_values = np.array(list(nx.degree_centrality(self.T).values()))
        self.T_weights_values = np.array(list(self.T_weights.values()))
        # mweights = minmax_scale(weights)
        # redblue = lambda p, q: (0,0,1) if p >= q else (1,0,0)
        # pal = [redblue(r, np.median(mweights)) for r in mweights]
        # print(pal)

    def set_corr_tree(self, data):
        self.matrix = _correlation(data, self.corr_mode, self.corr_type, self.graphical_alpha)
        if issparse(self.matrix): self.matrix = self.matrix.todense()
        self.G = nx.Graph(self.matrix)
        self.T = nx.minimum_spanning_tree(self.G)
        self.G_weights = {(u,v): np.abs(self.matrix[u,v]) for u,v in self.G.edges()}
        self.T_weights = {(u,v): np.abs(self.matrix[u,v]) for u,v in self.T.edges()}
        nx.set_edge_attributes(self.T,'weight',self.T_weights)
        self.G = nx.relabel_nodes(self.G, dict(enumerate(self.labels)))
        self.T = nx.relabel_nodes(self.T, dict(enumerate(self.labels)))
        self.node_weights = {i: nx.degree_centrality(self.T)[i] for i in self.G.nodes()}
        #self.T_order = OrderedDict(sorted(self.node_weights.items(), key=lambda x: x[1], reverse = True))

    def plot(self, *args,**kwargs):
        fig = self._draw(self.T, weights = self.T_weights, *args, **kwargs)
        return fig

    def mini_plots(self, radius = 2, root = None, center = False, fig = None, ax = None, *args, **kwargs):

        if root is None:
            num_plots = len(self.G.nodes())

            nrows, ncols = rectangle(num_plots)
            if fig is None: fig = plt.figure(figsize=(14,3*num_plots))
            root_list = self.T.nodes()
        else:
            if fig is None: fig, ax = plt.subplots(figsize=(7,3))
            root_list = [root]
            nrows, ncols = 1, 1
        np.random.seed(self.seed)
        for j, u in enumerate(root_list):

            H = nx.ego_graph(self.T, radius = radius, center = center, n = u, distance = 'weights')
            title = '{} ({}/{} connections)'.format(
                u,
                len(H.edges()),
                len(self.T.edges()),)
            if root is None: ax = fig.add_subplot(nrows,ncols,1+j)
            ax.set_title(title,size = 16)

            pos = nx.layout.spring_layout(H)
            labels =  H.nodes()
            nx.draw_networkx_edges(H, pos = pos, ax = ax, alpha = 0.3, width =   12*  self.T_weights_values)
            nx.draw_networkx_labels(H, pos = pos, ax = ax)
            plt.setp(ax, xticks=[], yticks=[], frame_on=True)
            #self._draw(H, fig = fig, ax = ax, *args, **kwargs)

    def _draw(self, graph, weights, fig = None, ax = None, layout_fun = nx.layout.spring_layout, node_size_fun = None, node_color_fun = None, edge_width_fun = None, cmap = cm.hot_r, alpha = 0.5):
        np.random.seed(self.seed)
        if ax is None:
            fig, ax = plt.subplots(figsize=(10,6))
        pos = layout_fun(graph)

        if node_size_fun is None: node_size_fun = lambda w: 100 + 1200 * w**2
        if node_color_fun is None: node_color_fun = lambda w : w**2
        if edge_width_fun is None: edge_width_fun = lambda w: 1 + 12 * w
        scaled_weights = minmax_scale(self.T_weights.values())
        _ = nx.draw_networkx_nodes(self.T, pos = pos,
                                    node_size = node_size_fun(self.T_node_weight_values),
                                    node_color = node_color_fun(minmax_scale(self.T_node_weight_values)),
                                    cmap=cmap,linewidths=0, alpha=alpha)
        _ = nx.draw_networkx_edges(self.T,pos=pos, width = edge_width_fun(scaled_weights), alpha = 0.3, ax = ax)
        _ = nx.draw_networkx_labels(self.T,pos=pos,size = 16, ax = ax)
        plt.setp(ax, xticks=[], yticks=[], frame_on=False)
        plt.tight_layout(h_pad=0.5, w_pad=0.01)
        return fig


# def _corr_digraph(data, biased = False):
#     G = _corr_graph(data, biased)
#     H = nx.DiGraph()
#     for x in G.nodes(): H.add_node(x)
#     for u,v,d in G.edges(data='weight'):
#         if d>0:
#             H.add_edge(u,v,weight=d)
#         else:
#             H.add_edge(v,u, weight = -d)
#     return H
